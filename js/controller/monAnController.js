export let monAnController = {
  layThongTinTuForm: () => {
    let tenMon = document.getElementById("name").value;
    let giaMon = document.getElementById("price").value;
    let moTa = document.getElementById("description").value;
    let anhMon = document.getElementById("img").value;

    let monAn = {
      name: tenMon,
      price: giaMon,
      description: moTa,
      img: anhMon,
    };
    return monAn;
  },
  showThongLenForm: (monAn) => {
          document.getElementById("name").value = monAn.name;
          document.getElementById("price").value = monAn.price;
          document.getElementById("description").value = monAn.description;
          document.getElementById("img").value = monAn.img;
  }
};
