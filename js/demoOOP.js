// before

let Cat = function (name, age) {
  this.name = name;
  this.age = age;
};

let tom = new Cat("Tom", 12);
console.log("tom : ", tom);

// after

class Dog {
  constructor(name, age) {
    this.name = name;
    this.age = age;
  }
  sayHello() {
    [(this.name = "plot")];
  }
}

let pull = new Dog("Pull", 1.2);
console.log("pull: ", pull);
console.log("pull: ", pull.sayHello);
