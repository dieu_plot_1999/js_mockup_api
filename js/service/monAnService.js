const BASE_URL = "https://62b07879e460b79df0469ba7.mockapi.io/mon-an";
export let monAnService = {
  layDanhSachMonAn: () => {
    return axios({
      url: BASE_URL,
      method: "GET",
    });
  },
  xoaMonAn: (id) => {
    return axios({
      url: `${BASE_URL}/${id}`,
      method: "DELETE",
    });
  },
  themMonAn: (monAn) => {
    return axios({
      url: BASE_URL,
      method: "POST",
      data: monAn,
    });
  },
  layChiTietMonAn: (idMonAn) => {
    return axios({
      url: `${BASE_URL}/${idMonAn}`,
      method: "GET",
    });
  },
  suaMonAn: (monAn) => {
    return axios({
      url: `${BASE_URL}/${monAn.id}`,
      method: "PUT",
      data: monAn,
    });
  },
};

//default: 1 lần duy nhất 1 trong file
