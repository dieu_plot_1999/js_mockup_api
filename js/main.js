//https://62b07879e460b79df0469ba7.mockapi.io/
import {monAnController} from "./controller/monAnController.js";
import {monAnService} from "./service/monAnService.js";
import {spinnerService} from "./service/spinnerService.js";

let foodList = [];
let idFoodEdited = null;

let renderTable = (list) => {
  //render ra ngoài
  let contentHTML = "";
  for (let index = 0; index < list.length; index++) {
    // contentHTML += [index];
    let monAn = list[index];
    let contentTr = `
        <tr>
            <td>${monAn.id}</td>
            <td>${monAn.name}</td>
            <td>${monAn.price}</td>
            <td>${monAn.description}</td>
            <td>
            <img src="${monAn.img}" alt="không có hình" style="width:100px"/>
            </td>
            <td>
                <div class="d-flex p-2" >
                <button onclick="layChiTietMonAn(${monAn.id})" class="btn btn-primary" style="margin-right:15px">Sửa</button>
                <button onclick="xoaMonAn(${monAn.id})" class="btn btn-warning">Xóa</button>
                </div>
            </td>
        </tr>
        `;
    contentHTML = contentHTML + contentTr;
  }
  document.getElementById("tbodya_food").innerHTML = contentHTML;
};

let xoaMonAn = (maMonAn) => {
  spinnerService.batLoading();
  monAnService
    .xoaMonAn(maMonAn)
    .then((res) => {
      spinnerService.tatLoading();
      console.log(res);
      renderDAnhSach();
    })
    .catch((err) => {
      spinnerService.tatLoading();
      console.log("err", err);
    });
};

window.xoaMonAn = xoaMonAn;

let renderDAnhSach = () => {
  spinnerService.batLoading();
  monAnService
    .layDanhSachMonAn()
    .then((res) => {
      spinnerService.tatLoading();
      foodList = res.data;
      renderTable(foodList);
      // console.log(res.data);
    })
    .catch((error) => {
      spinnerService.tatLoading();

      console.log(error);
    });
  // console.log(foodList);
};
renderDAnhSach();
//callback queue
//setTimeout(() =>)

// thêm món ăn

let themMonAn = () => {
  let monAn = monAnController.layThongTinTuForm();
  spinnerService.batLoading();
  monAnService
    .themMonAn(monAn)
    .then((res) => {
      // alert("Thêm thành công!");
      spinnerService.tatLoading();
      renderDAnhSach();
    })
    .catch((err) => {
      spinnerService.tatLoading();
    });
  // console.log(tenMon, giaMon, moTa);
};

window.themMonAn = themMonAn;

let layChiTietMonAn = (idMonAn) => {
  idFoodEdited = idMonAn;
  spinnerService.batLoading();
  monAnService
    .layChiTietMonAn(idMonAn)
    .then((res) => {
      console.log("Chi tiet mon ăn", res);
      // lấy dữ liệu show lên giao diện(binding dữ liệu (data binding))
      monAnController.showThongLenForm(res.data);
      document.getElementById("btn-sua").style.display = "block";
      document.getElementById("btn-them").style.display = "none";
      spinnerService.tatLoading();
    })
    .catch((err) => {
      console.log("lỗi!", err);
      spinnerService.tatLoading();
    });
};
window.layChiTietMonAn = layChiTietMonAn;

let suaMonAn = () => {
  let monAn = monAnController.layThongTinTuForm();

  console.log("monAn", monAn);
  spinnerService.batLoading();

  let newMonAn = {...monAn, id: idFoodEdited};

  monAnService
    .suaMonAn(newMonAn)
    .then((res) => {
      document.getElementById("btn-sua").style.display = "none";
      document.getElementById("btn-them").style.display = "block";
      spinnerService.tatLoading();
      console.log(res);
      monAnController.showThongLenForm({
        name: "",
        price: "",
        description: "",
        img: "",
      });
      renderDAnhSach();
    })
    .catch((err) => {
      spinnerService.tatLoading();
      console.log(err);
    });
};

window.suaMonAn = suaMonAn;
